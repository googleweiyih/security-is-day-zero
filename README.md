## How to avoid commitiung secret keys?

### gitleaks
https://github.com/gitleaks/gitleaks


### git-secrets

Do a manual scan by settint as follows:

To setup:
```bash
brew install git-secrets

cd repo_path

# Do this for every new git repo
git secrets --install
# HF
git secrets --add --global 'hf_[a-zA-Z0-9]{34}'
# OpenAI
git secrets --add --global '(ghp_|gho_|ghu_|ghs_|ghr_)[a-zA-Z0-9]{36}'
# Google Service Service account key
git secrets --add --global '"private_key": "-----BEGIN PRIVATE KEY-----\n[a-zA-Z0-9]{30}' # pragma: allowlist secret

# Do a manual scan
git secrets --scan

```

### detect-secrets

To setup:
```bash
brew install detect-secrets
```

Add pre-commit config
```yaml
repos:
-   repo: https://github.com/Yelp/detect-secrets
    rev: v1.5.0
    hooks:
    -   id: detect-secrets
        # args: ['--baseline', '.secrets.baseline']
        exclude: package.lock.json
```

Install pre-commit
```bash
pre-commit install 
pre-commit run --all-files

```

Inline allow whitelist
```python
API_KEY = "abc" # pragma: allowlist secret
```

